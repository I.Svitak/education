"use strict";
// щоб не писати однаковий код багато разів де це буде потрібно, а просто викликати функцію
// щоб передати інформацю для функції з якою потрібно працювати
// return повертає результат виконання функції щоб цей результам можна було записати поза функцією

let firstNum;
let secondNum;
let operator;
let result;

do {
  firstNum = +prompt("Enter first number!");
  secondNum = +prompt("Enter second number!");
  operator = prompt("Enter operator!");
} while (isNaN(firstNum, secondNum));

function calculator(numOne, numTwo, oper) {
  if (oper === "+") {
    result = numOne + numTwo;
    return result;
  } else if (oper === "-") {
    result = numOne - numTwo;
    return result;
  } else if (oper === "*") {
    result = numOne * numTwo;
    return result;
  } else if (oper === "/") {
    result = numOne / numTwo;
    return result;
  }
}

result = calculator(firstNum, secondNum, operator);
console.log(result);
