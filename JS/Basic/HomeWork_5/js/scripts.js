// методи це функції які зберігаються у властивостях об'єкта
// string, number, boolean...
// всі об'єкти мають посилання на якусь конкретну комірку пам'яті, тому виникають деякі складності з копіюванням об'єктів

"use strict";

function createNewUser() {
  do {
    this.firstName = prompt("Enter your first name!");
  } while (this.firstName === "" || !isNaN(this.firstName));
  do {
    this.lastName = prompt("Enter your last name!");
  } while (this.lastName === "" || !isNaN(this.lastName));
  this.getLogin = function () {
    let userLogin = (this.firstName[0] + this.lastName).toLowerCase();
    return userLogin;
  };
}
let newUser = new createNewUser();

console.log(newUser.getLogin());
