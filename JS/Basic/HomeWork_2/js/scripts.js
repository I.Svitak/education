// number, bigint, string, boolean, undefined, null, object
// == не строге порівняння. === строге порівняння з урахуванням типу даних
// оператор - елементи коду за допомогою яких виконуються операції над даними

"use strict";
let userName;
let userAge;
let notAllowed = "You are not allowed to visit this website.";

do {
  userName = prompt("Enter your name:", userName);
  userAge = prompt("Enter your age:", userAge);

  if (userAge >= 22) {
    alert(`Welcome, ${userName}`);
  } else if (userAge < 18) {
    alert(notAllowed);
  } else if (userAge >= 18 || userAge <= 22) {
    let confirmation = confirm("Are you sure you want to continue?");
    alert(confirmation ? `Welcome, ${userName}` : notAllowed);
  }
} while (isNaN(userAge) || userName == "");
