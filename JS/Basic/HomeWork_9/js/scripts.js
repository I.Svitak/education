// document.createElement('вказати назву тегу')
// перший параметр визначає місце вставки html. beforebegin - перед елементом. afterbegin - в елементі на початку. beforeend - в елементі вкінці. afterend - після елементу
// видалити можна за допомогою el.remove()
"use strict";
const city = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const data = ["1", "2", "3", "sea", "user", 23];
const arr3 = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin", ["Polonne", "Stryi"], "Ternopil"],
  "Odessa",
  "Lviv",
  "Dnipro",
];

function addList(array, element, seconds = 3) {
  if (!array || (Array.isArray(array) && !array.length)) {
    return;
  }
  const container = element || document.body;

  const createTimer = () => {
    const timer = document.createElement("p");
    timer.classList.add("timer");
    timer.innerHTML = seconds;
    return timer;
  };

  const timer = createTimer();
  container.appendChild(timer);

  const counter = () => {
    timer.innerHTML = seconds - 1;
    seconds--;
    if (seconds <= 0) {
      clearInterval(interval);
      timer.remove();
      list.remove();
    }
  };

  const interval = setInterval(counter, 1000);

  const createList = (items) => {
    const list = document.createElement("ul");
    items.filter(Boolean).forEach((item) => {
      const listItem = document.createElement("li");
      listItem.innerHTML = item;
      list.appendChild(Array.isArray(item) ? createList(item) : listItem);
    });
    return list;
  };
  const list = createList(array);
  container.appendChild(list);
}

addList(arr3);
addList(data, null, 5);
