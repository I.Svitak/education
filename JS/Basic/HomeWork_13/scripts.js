const pictures = Array.from(document.querySelectorAll(".image-to-show"));
const showClass = "show";
let interval;

function addClass() {
  let index = pictures.findIndex((element) =>
    element.classList.contains(showClass)
  );
  pictures.forEach((element) => element.classList.remove(showClass));
  if (index++ === pictures.length - 1) {
    index = 0;
  }
  pictures[index ? index++ : 0].classList.add(showClass);
}

function stopSlider() {
  clearInterval(interval);
}

function startSlider() {
  interval = setInterval(addClass, 1000);
}

addClass();
startSlider();
