// сучасні способи оголошення змінних - let, const. застарілий - var
// функція prompt виводить в браузері модальне вікно з текстовим полем для вводу
// неявне перетворення типів це автоматичне перетворення типів даних рушійом js. наприклад коли у вікно prompt ввести число, воно перетвориться у рядок

"use strict";

let name = "Ihor";
let admin = name;
console.log(admin);

let days = 2;
let secInDay = 24 * 3600;
console.log(days * secInDay);

let userText = prompt("Enter your text:");
console.log(userText);
