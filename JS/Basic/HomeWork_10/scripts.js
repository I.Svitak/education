//
//
//

const tabsTitle = document.querySelectorAll(".tabs-title");
const tabContent = document.querySelectorAll(".tabs-content li");
const activeClass = "active";

Array.from(tabsTitle).forEach((element) => {
  element.addEventListener("click", ({ target }) => {
    const classList = target.classList;
    const tabIndex = target.dataset.tabindex;
    const content = Array.from(tabContent).find(
      (li) => li.dataset.tabindex === tabIndex
    );

    if (Array.from(classList).includes(activeClass)) {
      return;
    }
    Array.from(tabsTitle).forEach((element) => {
      element.classList.remove(activeClass);
    });

    Array.from(tabContent).forEach((element) => {
      element.classList.remove(activeClass);
    });
    classList.add(activeClass);
    content.classList.add(activeClass);
  });
});
