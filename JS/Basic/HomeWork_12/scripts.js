const keys = Array.from(document.querySelectorAll(".btn"));
const changeButton = "active-btn";

document.addEventListener("keydown", (event) => {
  const findElement = (elements) =>
    elements.find(
      (elem) => elem.innerHTML.toLowerCase() === event.key.toLowerCase()
    );
  const button = findElement(keys);
  keys.forEach((element) => {
    element.classList.remove(changeButton);
  });
  button.classList.add(changeButton);
});
