// Перебирає масив та виконує колбек функцію для кожного елементу.
// arr.length = 0; arr = []; arr.splice(0, arr.length)
// arr.isArray(); - повертає true або false
"use strict";

function filterBy(dataType, ...data) {
  let newArr = [...data];
  let filter;
  if (dataType === "string") {
    filter = newArr.filter(function (element) {
      return typeof element !== "string";
    });
    return filter;
  } else if (dataType === "number") {
    filter = newArr.filter(function (element) {
      return typeof element !== "number";
    });
    return filter;
  } else if (dataType === "boolean") {
    filter = newArr.filter(function (element) {
      return typeof element !== "boolean";
    });
    return filter;
  } else if (dataType === "object") {
    filter = newArr.filter(function (element) {
      return typeof element !== "object";
    });
    return filter;
  } else if (dataType === "undefined") {
    filter = newArr.filter(function (element) {
      return typeof element !== "undefined";
    });
    return filter;
  } else if (dataType === "null") {
    filter = newArr.filter(function (element) {
      return typeof element !== null;
    });
    return filter;
  }
  return newArr;
}

let result = filterBy(
  "undefined",
  1,
  "2",
  null,
  24,
  undefined,
  "Hello",
  52,
  true,
  { name: "Ihor" },
  62,
  "Lviv",
  [3, "seven", 43],
  false,
  "PCM"
);
console.log(result);
