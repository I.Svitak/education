// екранування потрібно для того щоб вставляти спеціальні символи як звичайні
// оголошення функції через присвоєння її у змінну, та оголошення функції з особистим ім'ям
// hoisting підняття змінних або функцій, тобто функція оголоценна function declaration доступна до її оголошення, змінні let const до їх оголошення не мають ініціалізації, а неоголошений var має undefined

"use strict";

function createNewUser() {
  do {
    this.firstName = prompt("Enter your first name!");
  } while (this.firstName === "" || !isNaN(this.firstName));
  do {
    this.lastName = prompt("Enter your last name!");
  } while (this.lastName === "" || !isNaN(this.lastName));
  this.userBirthDay = prompt("Enter your birthday! format dd.mm.yyyy");
  this.birthDay = new Date(
    this.userBirthDay.slice(6),
    this.userBirthDay.slice(3, 5) - 1,
    this.userBirthDay.slice(0, 2)
  );
  this.today = new Date();
  this.birthdayThisYear = new Date(
    this.today.getFullYear(),
    this.birthDay.getMonth(),
    this.birthDay.getDate()
  );
  this.getAge = function () {
    let age = this.today.getFullYear() - this.birthDay.getFullYear();
    if (this.today < this.birthdayThisYear) {
      age = age - 1;
    }
    return age;
  };
  this.today = new Date();
  this.getLogin = function () {
    let userLogin = (this.firstName[0] + this.lastName).toLowerCase();
    return userLogin;
  };
  this.getPassword = function () {
    let userPassword =
      this.firstName[0].toUpperCase() +
      this.lastName.toLowerCase() +
      this.birthDay.getFullYear();
    return userPassword;
  };
}
let newUser = new createNewUser();
