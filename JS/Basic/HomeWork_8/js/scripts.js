// DOM це об'єктне подання документа, яке зв'язує його з JS та формує взаємодію, подається у вигляді вузлів елементів, дочірніх та батьківських елементів
// innerText показує тільки внутрішній текст елемента, без синтаксису, а innerHTML виводить повністю строку з текстом всередині та текстом розмітки тегів
// Найкращий спосіб querySellector так як він сучасніший та більш універсальний за устарівші getElementsBy. Відмінність у живій та не живій колекції у querySelector

"use strict";
let paragraphs = document.getElementsByTagName("p");
Array.from(paragraphs).forEach((element) => {
  element.style.color = "#ff0000";
});

let optList = document.querySelector("#optionsList");
console.log(optList);
console.log(optList.parentElement);
let optListNodes = optList.childNodes;

for (const node of optListNodes) {
  if (node.nodeName !== "#text") {
    console.log(`Type of Node: ${node.nodeType}, Node name: ${node.nodeName}`);
  }
}

let testParag = document.querySelector("#testParagraph");
console.log(testParag);
testParag.innerHTML = "This is a paragraph";

let mainHeader = document.querySelector(".main-header").children;
console.log(mainHeader);

Array.from(mainHeader).forEach((element) => {
  element.classList.add("nav-item");
});

let sectTitle = document.querySelectorAll(".section-title");

Array.from(sectTitle).forEach((element) => {
  element.classList.remove("section-title");
});
