const inputs = Array.from(document.querySelectorAll("input"));
const icons = Array.from(document.querySelectorAll("i"));
const error = document.querySelector(".error");
const changeIcon = "fa-eye-slash";

function toggleVisibility(event) {
  const findElement = (elements) =>
    elements.find((elem) => elem.dataset.pass === event.target.dataset.pass);
  const icon = findElement(icons);
  const input = findElement(inputs);
  input.setAttribute(
    "type",
    icon.classList.contains(changeIcon) ? "password" : "text"
  );
  icon.classList.toggle(changeIcon);
}

function checkPass() {
  const password = inputs[0].value;
  if (inputs.every((elem) => elem.value === password)) {
    alert("You are welcome");
    error.style = "Display: none";
    return;
  }
  error.style = "Display: block";
}
