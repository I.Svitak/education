const darkTheme = "dark-theme";

function toggleTheme() {
  document.body.classList.toggle(darkTheme);
  if (document.body.classList.contains(darkTheme)) {
    localStorage.setItem(darkTheme, true);
  } else {
    localStorage.removeItem(darkTheme);
  }
}

window.onload = () => {
  if (localStorage.getItem(darkTheme)) {
    document.body.classList.add(darkTheme);
  }
};
