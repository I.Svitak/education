const buttons = document.querySelectorAll(".button");

const images = [
  ...Array(12)
    .fill("")
    .map((i, idx) => `./img/our_work/graphic-design${idx + 1}.jpg`),
  ...Array(7)
    .fill("")
    .map((i, idx) => `./img/our_work/web-design${idx + 1}.jpg`),
  ...Array(7)
    .fill("")
    .map((i, idx) => `./img/our_work/landing-page${idx + 1}.jpg`),
  ...Array(10)
    .fill("")
    .map((i, idx) => `./img/our_work/wordpress${idx + 1}.jpg`),
];

const randomInitialPhotos = (photos) =>
  (photos || images).sort(() => 0.5 - Math.random()).slice(0, 12);

function loadPhotos(photos) {
  const container = document.querySelector(".work-photos");
  container.innerHTML = photos
    .map((p) => {
      return `
		<div class="photo-hover">
			<div class="photo-hover-icons">
				<div class="icon-one"></div>
				<div class="icon-two"></div>
			</div>
			<img src="${p}" alt="">
			<p class="photo-hover-title">creative design</p>
			<p class="photo-hover-subtitle">Web Design</p>
		</div>
	`;
    })
    .join("");
}

function photoFilter() {
  function filter(category) {
    if (category === "all") {
      loadPhotos(randomInitialPhotos());
      return;
    }
    const filtered = images.filter((i) => i.includes(category));
    loadPhotos(filtered);
  }

  buttons.forEach((button) => {
    button.addEventListener("click", (e) => {
      buttons.forEach((b) => b.classList.remove("active"));
      button.classList.add("active");
      const currentCategory = button.dataset.filter;
      filter(currentCategory);
      loadMoreBtn.style.display = "initial";
    });
  });
}

// Our Amazing work load more
const loadMoreBtn = document.querySelector(".load-more");

loadMoreBtn.addEventListener("click", () => {
  const activeTab = document.querySelector(".our-work-buttons .button.active");
  const visibleImages = Array.from(
    document.querySelectorAll(".work-photos .photo-hover img")
  ).map((e) => `./img${e.src.split("img")[1]}`);
  const nextImages = images
    .filter((i) => !visibleImages.includes(i))
    .filter((i) => {
      const filter = activeTab.dataset.filter;
      return filter === "all" || i.includes(filter);
    });

  if (!nextImages.length) {
    loadMoreBtn.style.display = "none";
    return;
  }

  loadPhotos(visibleImages.concat(randomInitialPhotos(nextImages)));
});

loadPhotos(randomInitialPhotos());
photoFilter();
