const root = document.querySelector("#root");
const addPost = document.querySelector(".btn__add");

class Card {
  constructor(post) {
    this.post = post;
    this.element = this.newCard();
  }

  newCard() {
    const { title, body, userId } = this.post;
    const user = users.find((user) => user.id === userId);

    const card = document.createElement("div");
    card.classList.add("card");
    card.insertAdjacentHTML(
      "beforeend",
      `
			<div class="card__header">
				<div class="card__info">
					<h3 class="card__name">${user.name}</h3>
					<div class="card__username">@${user.username}</div>
					<div class="card__email">${user.email}</div>
				</div>
			</div>	
			<div class="card__block">
				<h4 class="card__title">${title}</h4>
				<p class="card__body">${body}</p>
		   </div>`
    );

    const deleteCard = document.createElement("button");
    deleteCard.classList.add("btn__delete");
    deleteCard.textContent = "Delete";
    card.append(deleteCard);
    deleteCard.addEventListener("click", this.deleteCard.bind(this));
    return card;
  }

  async deleteCard() {
    try {
      await fetch(
        `https://ajax.test-danit.com/api/json/posts/${this.post.id}`,
        {
          method: "DELETE",
        }
      );
      this.element.remove();
    } catch (error) {
      console.log(error);
    }
  }
}

async function getData() {
  const [usersRes, postsRes] = await Promise.all([
    await fetch("https://ajax.test-danit.com/api/json/users", {
      method: "GET",
    }),
    await fetch("https://ajax.test-danit.com/api/json/posts", {
      method: "GET",
    }),
  ]);
  users = await usersRes.json();
  posts = await postsRes.json();

  posts.forEach((post) => {
    const card = new Card(post);
    root.append(card.element);
  });
}

getData();
