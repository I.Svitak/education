//  Кожен об'єкт у js має свій prototype, в якому знаходяться його методи. Якщо певний екземпляр створений на основі іншого об'єкта, він має посилання на батьківський елемент і може використовувати його методи, якщо не знайде їх у себе
// 	Super() дозволяє використовувати функції батьківського конструктора у створенні нового класу розширеного іншим

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  set name(newName) {
    return (this._name = newName);
  }

  get age() {
    return this._age;
  }

  set age(newAge) {
    return (this._age = newAge);
  }

  get salary() {
    return this._salary;
  }

  set salary(newSalary) {
    return (this._salary = newSalary);
  }
}

const employee1 = new Employee("Ihor", 26, 1200);

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get lang() {
    return (this._lang = lang);
  }

  set lang(newLang) {
    return (this._lang = newLang);
  }

  get salary() {
    return super.salary * 3;
  }
}

const programmer1 = new Programmer("Aliona", 25, 2500, "en, ua, ge");
