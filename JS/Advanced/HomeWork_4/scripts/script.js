// AJAX спосіб обміну даними між клієнтом та сервером, основною перевагою є те що це дозволяє отримувати та надсилати дані на сервер без перезавантаження сторінки

const requestUrl = "https://ajax.test-danit.com/api/swapi/films";

function getData(url) {
  return new Promise((resolve, reject) => {
    fetch(url).then((response) => {
      if (!response.ok) {
        reject(response.statusText);
      }
      resolve(response.json());
    });
  }).catch((error) => console.error("Fetch error: ", error));
}

function createList(data, container = document.body) {
  const list = document.createElement("ul");
  container.append(list);

  return data.map((element) => {
    const item = document.createElement("li");
    item.innerHTML = element.template;
    list.append(item);
    return {
      ...element,
      node: item,
    };
  });
}

function getMultipleData(data) {
  return Promise.all(
    (data ?? []).map((url) => {
      return getData(url);
    })
  );
}

const episodTemplate = (episode) => {
  return `<h2>Episode name: ${episode.name}</h2> <br>
<strong>Episode ID: </strong>${episode.id} <br>
<strong>Description:</strong> ${episode.openingCrawl}`;
};

const characterTemplate = (character) => {
  return `
		<p>Name: ${character.name}</p>`;
};

getData(requestUrl).then((data) => {
  console.log(data);
  const episodes = createList(
    data.map((el) => ({ ...el, template: episodTemplate(el) }))
  );
  episodes.forEach((episode) => {
    getMultipleData(episode.characters).then((response) => {
      const characters = createList(
        response.map((el) => ({ ...el, template: characterTemplate(el) })),
        episode.node
      );
    });
  });
});
