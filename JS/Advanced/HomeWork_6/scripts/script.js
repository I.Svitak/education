// Асинхронність у js дозволяє виконувати код не послідовно а паралельно. JS виконує інші завдання тоді як функції чекають свого завершення. в основному це допомагає працювати з віддаленими даними

async function getIp() {
  const request = await fetch("https://api.ipify.org/?format=json");
  const response = await request.json();
  await getIAddress();
}

async function getIAddress() {
  const request = await fetch(
    "http://ip-api.com/json/?fields=continent,country,regionName,city,district"
  );
  const response = await request.json();
  console.log(response);
  createElement(response);
}

const button = document.querySelector("#btn");
button.addEventListener("click", getIp);

function createElement(address) {
  const list = document.createElement("ul");
  for (const key in address) {
    list.insertAdjacentHTML(
      "afterbegin",
      `<li>${key}: ${address[key] || "Data not available"}</li>`
    );
  }
  document.body.append(list);
}
