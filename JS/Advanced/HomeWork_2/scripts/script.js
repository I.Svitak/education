// В блок try поміщається код з потенційно можливою помилкою, якщо всетаки вона виникає, код переходить до блоку catch
// де можна вивести інформацію про помилку. і при цьому всьому скрипт не буде зупинятись якщо в try буде помилка

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.querySelector("#root");
const fields = ["author", "name", "price"];

function objectFilter(book, missFields) {
  book.forEach((bookItem, index) => {
    try {
      const missingFields = missFields.filter(
        (itemFields) => !(itemFields in bookItem)
      );
      if (missingFields.length === 0) {
        createList(bookItem);
      } else {
        throw new Error(
          `у книзі ${index + 1} відсутні властивості ${missingFields}`
        );
      }
    } catch (error) {
      console.log(`${error.name} - ${error.message}`);
    }
  });
}

function createList(bookItem, elem = document.body) {
  const listItem = document.createElement("li");
  const list = document.createElement("ul");
  elem.appendChild(list);
  listItem.innerHTML = `
        Автор: ${bookItem.author} </br>
        Ім'я: ${bookItem.name} </br>
        Ціна: ${bookItem.price} </br>
      `;
  list.appendChild(listItem);
}

objectFilter(books, fields);

// function createList(book) {

//   book.forEach((bookItem, index) => {
//     try {
//       const missingFields = fields.filter(
//         (itemFields) => !(itemFields in bookItem)
//       );
//       if (missingFields.length === 0) {
//         const listItem = document.createElement("li");
//         listItem.innerHTML = `
//         Автор: ${bookItem.author} </br>
//         Ім'я: ${bookItem.name} </br>
//         Ціна: ${bookItem.price} </br>
//       `;
//         list.appendChild(listItem);
//       } else {
//         throw new Error(
//           `у книзі ${index + 1} відсутні властивості ${missingFields}`
//         );
//       }
//     } catch (error) {
//       console.log(`${error.name} - ${error.message}`);
//     }
//   });
// }

// createList(books);
