const links = document.querySelectorAll(".menu__link");
const active = "active";

links.forEach((element) => {
  element.addEventListener("click", (element) => {
    const classList = element.target.classList;
    if (Array.from(classList).includes(active)) {
      return;
    }

    links.forEach((element) => {
      element.classList.remove(active);
    });

    element.target.classList.add(active);
  });
});
