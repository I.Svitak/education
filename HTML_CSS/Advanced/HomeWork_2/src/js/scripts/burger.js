const burgerBtn = document.querySelector(".burger-btn");
const burgerMenu = document.querySelector(".menu__list");
const activeBtn = "active-btn";
const activeMenu = "active-menu";

function toggleClass() {
  burgerMenu.classList.toggle(activeMenu);
  burgerBtn.classList.toggle(activeBtn);
}

burgerBtn.addEventListener("click", toggleClass);

burgerMenu.addEventListener("click", (event) => {
  const classLs = event.target.classList;
  console.log(classLs);
  console.log(event);
});
