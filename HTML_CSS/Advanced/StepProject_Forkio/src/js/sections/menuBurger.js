const iconBurger = document.querySelector('.burger');
const menuBody = document.querySelector('.menu');

if (iconBurger) {
	document.body.addEventListener('click', (e) => {
		const navMenu = e.composedPath().includes(menuBody);
		
		if (e.target.closest('.burger') || e.target.closest('.menu__link')) {			
			iconBurger.classList.toggle('burger--active');
			menuBody.classList.toggle('menu--active');
		} else if (!navMenu) {
			e.preventDefault();
			menuBody.classList.remove('menu--active');
			iconBurger.classList.remove('burger--active');
		} 
	})
}